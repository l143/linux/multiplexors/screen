# Screen

Screen es un multiplexor de terminal.

---
## ⚒ Comandos útiles.
<br>

> Instalar con apt
```
$ sudo apt-get install screen
```

> Crear una sesión
```
$ screen -S [sessionName]
```

> Salir de una sesión
```
Presionar Ctrl + a + d
```

> Listar las sesiones
```
$ screen -ls
```

> Retomar una sesión  
*Si solo existe una sesión, no es necesario inicar el sessionID*
```
$ screen -r [sessionID]
```

> Eliminar una sesión
```
$ screen -XS [sessionId] quit
```

## Referencias
- https://linuxize.com/post/how-to-use-linux-screen/
- https://askubuntu.com/questions/356006/kill-a-screen-session